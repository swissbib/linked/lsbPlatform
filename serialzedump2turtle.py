#!/usr/bin/env python3.5

from os import path, listdir, remove, mkdir
from shutil import rmtree, move
import sys
import logging
import json
import subprocess
import tempfile
from time import monotonic, strftime, localtime
from math import floor
import argparse
from elasticsearch import Elasticsearch, exceptions
import re
from jsmin import jsmin
from time import sleep
from neo4j.v1 import GraphDatabase, basic_auth

__author__ = 'Günter Hipler'
__copyright__ = 'Copyright 2018, swissbib project, UB Basel'
__license__ = 'http://opensource.org/licenses/gpl-2.0.php'
__version__ = '1'
__maintainer__ = 'Günter Hipler'
__email__ = 'guenter.hipler@unibas.ch'
__status__ = 'development, based on initwf.py from sebastian.schuepbach@unibas.ch'


class Stopwatch(object):
    """
    An implementation of a simple stopwatch with formatted output of the delta time.
    """

    def __init__(self):
        self.starttime = monotonic()

    def stop(self):
        delta = monotonic() - self.starttime
        floored = floor(delta)
        hrs = floored // 3600
        minutes = (floored % 3600) // 60
        sec = delta % 60
        return '{0}h {1}m {2:.3f}s'.format(hrs, minutes, sec)




def subroutine(arglist, cwd='.'):
    """
    Wraps a call to the run method of package subprocess
    :param arglist: List with command and arguments
    :param cwd: Set working directory
    :return: A CompletedProcess instance
    """
    p = subprocess.run(arglist, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    logging.info(p.stdout.decode("utf-8"))
    return p


def vartemplate(filename, workflow):
    """
    Replaces variables in a FLUX template and saves file in a temporary file
    :param filename: Path to FLUX template
    :param workflow: Name of workflow
    :return: Path to the newly created file
    """
    tmpprefix = '{}_{}_'.format(strftime('%Y%m%d%H%M%S', localtime()), workflow)
    fd, filepath = tempfile.mkstemp(prefix=tmpprefix)
    with open(filepath, 'w') as out:
        with open(filename) as fl:
            content = ""
            tempstr = ""
            invar = False
            while True:
                c = fl.read(1)
                if not c:
                    break
                elif c is '[':
                    invar = True
                elif c is ']':
                    content += '"'
                    content += cfg[tempstr]
                    content += '"'
                    tempstr = ""
                    invar = False
                elif invar:
                    tempstr += c
                else:
                    content += c
        out.write(content)
    return filepath


def checkfileexists(filelist):
    """
    Checks if files in given list exist in file system. Quits if errors are found
    :param filelist: List of files to check
    :return: None
    """
    for sf in filelist:
        logmsg = 'Checking if file ' + sf + ' exists... '
        if not path.exists(sf):
            logging.error(logmsg + 'FAILED. Aborting.')
            sys.exit(1)
        else:
            logging.debug(logmsg + 'OK')




parser = argparse.ArgumentParser()
parser.add_argument('-c', '--config', help='Path to configuration file', type=str, default='conf.json')
parser.add_argument('-l', '--log', help='Path to logging directory', type=str, default='log')
parser.parse_args()


args = parser.parse_args()

# Don't perform certain steps if initialLoad is set to False

logfilename = (args.log if args.log.endswith('/') else args.log + '/') + 'initwf_' + strftime('%Y%m%d%H%M', localtime())
logging.basicConfig(filename=logfilename, format='%(asctime)s %(message)s', level=logging.DEBUG, filemode='a')

with open(args.config) as cfgfile:
    cfg = json.load(cfgfile)

fluxsh = cfg['mfhome'] + 'flux.sh' if cfg['mfhome'].endswith('/') else cfg['mfhome'] + '/flux.sh'

linkingreturncode = 0

ta = Stopwatch()

# Create new index in initialload workflow

logging.info("starts serialzation from jsonld to turtle")
checkfileexists(
    [cfg['serialize2turtle']])
t = Stopwatch()
srargs = ['bash', fluxsh, vartemplate(cfg['serialize2turtle'], 'serialze_turtle')]
subroutine(srargs, cfg['mfhome'])
logging.info('Serialization of dump finished. Took {}'.format(t.stop()))



