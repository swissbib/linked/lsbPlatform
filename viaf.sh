#!/usr/bin/env bash


#docker start elasticsearch
#docker run -d --name elasticsearch \
#        -v /home/jonas/projects/metafacture/lsbPlatform/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
#        -v /home/jonas/linked/data:/usr/share/elasticsearch/data \
#        -p 9200:9200 -p 9300:9300 --ulimit nofile=65536:65536 elasticsearch:5.6.12


#docker run -d --name kibana \
#    -v /home/jonas/projects/metafacture/lsbPlatform/kibana.yml:/usr/share/kibana/config/kibana.yml \
#    -p 5601:5601 kibana:5.6.12

# docker start metafacture


docker run -d --name metafacture \
    -v /home/jonas/linked/data/enrichedLineOutput/viaf:/in:ro \
    -v /home/jonas/linked/data/enrichedLineOutput/output:/out \
    -v /home/jonas/projects/metafacture/lsbPlatform:/mfwf:ro \
    -v /home/jonas/projects/metafacture/swissbib-metafacture-commands/build/libs:/app/plugins:ro \
    --network host \
    sschuepbach/metafacture-runner \
    /mfwf/flux/viaf_process.flux