#!/usr/bin/env python3.5

from os import path, listdir, remove, mkdir
from shutil import rmtree, move
import sys
import logging
import json
import subprocess
import tempfile
from time import monotonic, strftime, localtime
from math import floor
import argparse
from elasticsearch import Elasticsearch, exceptions
import re
from jsmin import jsmin
from time import sleep
from neo4j.v1 import GraphDatabase, basic_auth

__author__ = 'Sebastian Schüpbach'
__copyright__ = 'Copyright 2016, swissbib project, UB Basel'
__license__ = 'http://opensource.org/licenses/gpl-2.0.php'
__version__ = '1'
__maintainer__ = 'Sebastian Schüpbach'
__email__ = 'sebastian.schuepbach@unibas.ch'
__status__ = 'development'


class Stopwatch(object):
    """
    An implementation of a simple stopwatch with formatted output of the delta time.
    """

    def __init__(self):
        self.starttime = monotonic()

    def stop(self):
        delta = monotonic() - self.starttime
        floored = floor(delta)
        hrs = floored // 3600
        minutes = (floored % 3600) // 60
        sec = delta % 60
        return '{0}h {1}m {2:.3f}s'.format(hrs, minutes, sec)


class ESBulkprocessor(object):
    """
    A wrapper around the bulk method of the Elasticsearch library.
    """

    def __init__(self, index, esconn, limit):
        self.limit = limit
        self.esconn = esconn
        self.index = index
        self.statements = list()
        self.counter = 0

    def add(self, estype, esid):
        statement = self._buildstatement(estype, esid)
        self.statements.append(statement)
        self.counter += 1
        if self.counter >= self.limit:
            self._flush()
        return statement

    def close(self):
        if self.counter > 0:
            self._flush()

    def _buildstatement(self, estype, esid):
        return '{{"delete":{{"_index":"{}","_type":"{}","_id":"{}"}}}}' \
            .format(self.index, estype, esid)

    def _flush(self):
        self.esconn.bulk(body='\n'.join(self.statements), index=self.index)
        self.statements = list()
        self.counter = 0


def subroutine(arglist, cwd='.'):
    """
    Wraps a call to the run method of package subprocess
    :param arglist: List with command and arguments
    :param cwd: Set working directory
    :return: A CompletedProcess instance
    """
    p = subprocess.run(arglist, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    logging.info(p.stdout.decode("utf-8"))
    return p


def vartemplate(filename, workflow):
    """
    Replaces variables in a FLUX template and saves file in a temporary file
    :param filename: Path to FLUX template
    :param workflow: Name of workflow
    :return: Path to the newly created file
    """
    tmpprefix = '{}_{}_'.format(strftime('%Y%m%d%H%M%S', localtime()), workflow)
    fd, filepath = tempfile.mkstemp(prefix=tmpprefix)
    with open(filepath, 'w') as out:
        with open(filename) as fl:
            content = ""
            tempstr = ""
            invar = False
            while True:
                c = fl.read(1)
                if not c:
                    break
                elif c is '[':
                    invar = True
                elif c is ']':
                    content += '"'
                    content += cfg[tempstr]
                    content += '"'
                    tempstr = ""
                    invar = False
                elif invar:
                    tempstr += c
                else:
                    content += c
        out.write(content)
    return filepath


def checkfileexists(filelist):
    """
    Checks if files in given list exist in file system. Quits if errors are found
    :param filelist: List of files to check
    :return: None
    """
    for sf in filelist:
        logmsg = 'Checking if file ' + sf + ' exists... '
        if not path.exists(sf):
            logging.error(logmsg + 'FAILED. Aborting.')
            sys.exit(1)
        else:
            logging.debug(logmsg + 'OK')


def checkesconnection(index, httphost):
    """
    Checks if connection to Elasticsearch can be established and if a given index exists. Quits if errors are found
    :param index: Name of index
    :param httphost: Hosts and ports as string in the form of host1:port1#host2:port2#...#hostx:portx
    :return: None
    """
    try:
        les = Elasticsearch(httphost.split('#'))
    except exceptions.ConnectionError:
        logging.error('Could not connect to Elasticsearch cluster')
        sys.exit(1)
    if not les.indices.exists(index):
        logging.error(
            'Elasticsearch index ' + index + 'does not exist. Please create one by setting argument -i <configfile>')
        sys.exit(1)


def movecbsmessages(origdir, destdir):
    """
    Move messages from CBS into a temporary directory
    :param origdir: Path to origin directory
    :param destdir: Path to temporary directory
    :return: None
    """
    subroutine([path.join(cfg['tomcatbin'], "shutdown.sh"), "--force"])
    sleep(5)
    move(origdir, destdir)
    mkdir(origdir)
    subroutine([path.join(cfg['tomcatbin'], "startup.sh")])


parser = argparse.ArgumentParser()
parser.add_argument('-c', '--config', help='Path to configuration file', type=str, default='conf.json')
parser.add_argument('-l', '--log', help='Path to logging directory', type=str, default='log')
parser.add_argument('-i', '--indexconfig', help='Initialize index with designated config file',
                    type=argparse.FileType('r'))
parser.add_argument('-g', '--gc', help='Do additional garbage collecting', action='store_true')
parser.add_argument('-n', '--noenriching', help='Do not enrich concepts', action='store_true')
parser.add_argument('-t', '--tracking', help='Track changes in concepts bibliographicResource, person and organisation',
                    action='store_true')
parser.add_argument('actions',
                    help='''Actions to be performed. Valid actions are baseline, linking, enrindex, deleteline,
                workline, cleaning and all. Several actions are to be seperated by whitespace. Defaults to all''',
                    choices=['libadmin', 'baseline', 'linking', 'enrindex', 'deleteline', 'workline', 'all'],
                    default='all', nargs='*')
parser.parse_args()
args = parser.parse_args()

# Don't perform certain steps if initialLoad is set to False
initialLoad = False if args.indexconfig is None else True

logfilename = (args.log if args.log.endswith('/') else args.log + '/') + 'initwf_' + strftime('%Y%m%d%H%M', localtime())
logging.basicConfig(filename=logfilename, format='%(asctime)s %(message)s', level=logging.DEBUG, filemode='a')

with open(args.config) as cfgfile:
    cfg = json.load(cfgfile)

fluxsh = cfg['mfhome'] + 'flux.sh' if cfg['mfhome'].endswith('/') else cfg['mfhome'] + '/flux.sh'

linkingreturncode = 0

ta = Stopwatch()

# Create new index in initialload workflow
if initialLoad:
    logging.info('Creating new Elasticsearch index')
    try:
        body = jsmin(args.indexconfig.read())
        es = Elasticsearch(cfg['eshttphosts'].split('#'), sniff_on_start=False, sniff_on_connection_fail=True)
        es.indices.create(index=cfg['esindex'], body=body)
    except FileNotFoundError:
        logging.error("Could not load index settings file!")
        sys.exit(1)
    except exceptions.ConnectionError as ce:
        logging.error("Could not connect to Elasticsearch cluster")
        sys.exit(1)

# Indexes libadmin entries
if 'libadmin' in args.actions or 'all' in args.actions:
    logging.info("Starting libadmin workflow...")
    checkfileexists([cfg['mfhome'], cfg['libadmintmpl'], cfg['libadminmorph']])
    t = Stopwatch()
    checkesconnection(cfg['esindex'], cfg['eshttphosts'])
    srargs = ['bash', fluxsh, vartemplate(cfg['libadmintmpl'], 'libadmin')]
    subroutine(srargs, cfg['mfhome'])
    logging.info('libadmin workflow finished. Took {}'.format(t.stop()))

# Processes baseline workflow
if 'baseline' in args.actions or 'all' in args.actions:
    logging.info("Beginning baseline...")
    checkfileexists(
        [cfg['blin'] if initialLoad else cfg['crdir'], cfg['245afilter'], cfg['resomorph'], cfg['documorph'],
         cfg['itemmorph'], cfg['persmorph'],
         cfg['orgamorph'], cfg['blout'], cfg['bltmpl'], cfg['ibltmpl'], cfg['mfhome'], cfg['tomcatbin'],
         cfg['tibltmpl'], cfg['tibltmplne'], cfg['tcontribmorph'], cfg['tresmorph'], cfg['tdumpdir']])
    t = Stopwatch()
    if len(listdir(cfg['crdir'])) == 0 and not initialLoad:
        logging.info('Input directory empty, so nothing to do here.')
    else:
        checkesconnection(cfg['esindex'], cfg['eshttphosts'])
        if not initialLoad:
            movecbsmessages(cfg['crdir'], cfg['crdirtmp'])
        for f in listdir(cfg['blout']):
            remove(path.join(cfg['blout'], f))
        template = str()
        if initialLoad and args.noenriching and not args.tracking:
            logging.debug("""The following options are set:
            - Does initial loading
            - All concepts are directly imported into Elasticsearch (non-enriching workflow)
            - Tracking is disabled""")
            template = cfg['ibltmplne']
        elif not initialLoad and args.noenriching and not args.tracking:
            logging.debug("""The following options are set:
            - Does an update on an already existing index
            - All concepts are directly imported into Elasticsearch (non-enriching workflow)
            - Tracking is disabled""")
            template = cfg['bltmplne']
        elif initialLoad and not args.noenriching and not args.tracking:
            logging.debug("""The following options are set:
            - Does initial loading
            - Person and organisation are written to filesystem in order to enriching them
            - Tracking is disabled""")
            template = cfg['ibltmpl']
        elif not initialLoad and not args.noenriching and not args.tracking:
            logging.debug("""The following options are set:
            - Does an update on an already existing index
            - All concepts are directly imported into Elasticsearch (non-enriching workflow)
            - Tracking is disabled""")
            template = cfg['bltmpl']
        elif initialLoad and args.noenriching and args.tracking:
            logging.debug("""The following options are set:
            - Does initial loading
            - All concepts are directly imported into Elasticsearch (non-enriching workflow)
            - Tracking is enabled""")
            template = cfg['tibltmplne']
        elif initialLoad and not args.noenriching and args.tracking:
            logging.debug("""The following options are set:
            - Does initial loading
            - Person and organisation are written to filesystem in order to enriching them
            - Tracking is enabled""")
            template = cfg['tibltmpl']
        srargs = ['bash', fluxsh, vartemplate(template, 'baseline')]
        subroutine(srargs, cfg['mfhome'])
        if not initialLoad:
            rmtree(cfg['crdirtmp'])
    logging.info('Tranformation and indexing finished. Took {}'.format(t.stop()))

# Processes linking workflow
if ('linking' in args.actions or 'all' in args.actions) and not args.noenriching:
    logging.info('Starting linking process')
    checkfileexists([cfg['blout'], cfg['linking']])
    linkingbin = cfg['linking'] + 'execute_wf.sh' if cfg['linking'].endswith('/') else cfg['linking'] + '/execute_wf.sh'
    t = Stopwatch()
    linkingreturncode = subroutine(['bash', linkingbin], cfg['linking']).returncode
    if linkingreturncode == 0:
        logging.info('Linking successfully finished. Took {}'.format(t.stop()))
    elif initialLoad:
        logging.error('Linking aborted with return code {}. Took {}. ABORTING!'.format(linkingreturncode, t.stop()))
        exit(1)
    else:
        logging.warning(
            'Linking aborted with return code {}. Took {}. Continuing without further processing enriched '
            'output.'.format(
                linkingreturncode, t.stop()))

# Indexes output of linking workflow in Elasticsearch
if ('enrindex' in args.actions or 'all' in args.actions) and not args.noenriching and linkingreturncode == 0:
    logging.info('Indexing enriched person and organisation documents')
    checkfileexists(
        [cfg['elmorph'], cfg['elorgafilter'], cfg['elpersfilter'], cfg['elin'], cfg['eltmpl'], cfg['mfhome']])
    checkesconnection(cfg['esindex'], cfg['eshttphosts'])
    t = Stopwatch()
    subroutine(['bash', fluxsh, vartemplate(cfg['eltmpl'], 'enrichedline')], cfg['mfhome'])
    logging.info('Indexing of enriched person and organisation documents finished. Took {}'.format(t.stop()))

# Removes obsolete documents in types bibliographicResource, document and item
if ('deleteline' in args.actions or 'all' in args.actions) and not initialLoad:
    logging.info('Delete documents in index who have been removed in CBS')
    checkfileexists([cfg['dldir']])
    checkesconnection(cfg['esindex'], cfg['eshttphosts'])
    movecbsmessages(cfg['dldir'], cfg['dldirtmp'])
    t = Stopwatch()
    es = Elasticsearch(cfg['eshttphosts'].split('#'))
    bulkprocessor = ESBulkprocessor(cfg['esindex'], es, 500)
    pat = re.compile('^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}[-+]\d{4})_([^_]+)_([^.]+)\.xml')
    delmsgs = [f for f in listdir(cfg['dldirtmp']) if path.isfile(path.join(cfg['dldirtmp'], f))]
    for delmsg in delmsgs:
        resId = pat.match(delmsg).group(2)
        bibodelmsg = bulkprocessor.add('bibliographicResource', resId)
        docudelmsg = bulkprocessor.add('document', resId)
        logging.debug(bibodelmsg)
        logging.debug(docudelmsg)
        items = es.search(index=cfg['esindex'], doc_type='item',
                          body='{"query": {"term": {"bf:holdingFor": "http://data.swissbib.ch/resource/%s"}}}' %
                               resId, filter_path=['hits.hits._id'])
        if len(items) > 0:
            for i in items['hits']['hits']:
                itemdelmsg = bulkprocessor.add('item', i['_id'])
                logging.debug(itemdelmsg)

    bulkprocessor.close()
    rmtree(cfg['dldirtmp'])
    logging.info("Deleting finished. Took {}".format(t.stop()))

# Processes workline workflow
if 'workline' in args.actions or 'all' in args.actions:
    logging.info('Creating work concept')
    checkfileexists([cfg['wlbin']])
    checkesconnection(cfg['esindex'], cfg['eshttphosts'])
    logging.info("Deleting documents in type work")
    try:
        les = Elasticsearch(cfg['eshttphosts'].split('#'))
        les.delete_by_query(index=cfg['esindex'], doc_type='work', wait_for_completion=True,
                            body='{"query":{"match_all":{}}}', )
    except:
        logging.warning(
            "An exception has occurred. Since the operation is not vital for the integrity of the index, "
            "it is ignored.")
        logging.warning("Deleting documents aborted")
    else:
        logging.info("Finished deleting documents")
    t = Stopwatch()

    host, port = cfg['eshttphosts'].split('#')[0].split(':')
    srargs = [path.join(cfg['sparkhome'], 'bin/spark-submit'), '--class', 'org.swissbib.linked.Application',
              '--deploy-mode', 'client', '--conf', 'spark.ui.port=8081', '--conf', 'spark.es.nodes=' + host, '--conf',
              'spark.es.port=' + port, '--conf', 'spark.es.mapping.date.rich=false', '--conf',
              'spark.executor.memory=12g', '--conf', 'spark.blockManager.port=8008', '--conf',
              'spark.driver.port=7079', '--master', cfg['sparkmaster'], cfg['wlbin'], '--esIndex', cfg['esindex']]
    subroutine(srargs)
    logging.info('Creation of work concept finished. Took {}'.format(t.stop()))

# Processes garbage collection
if args.gc:
    logging.info('Garbage collecting index')
    checkfileexists([cfg['gcbin']])
    checkesconnection(cfg['esindex'], cfg['eshttphosts'])
    t = Stopwatch()
    srargs = ['java', '-jar', cfg['gcbin'], '-clean', '-eshost', cfg['estcphosts'].split('#')[0], '-esname',
              cfg['escluster'], '-esindex', cfg['esindex']]
    subroutine(srargs)
    logging.info('Garbage collecting finished. Took {}'.format(t.stop()))

# Processes tracking
if args.tracking and initialLoad:
    logging.info('Creating Neo4j database from csv dump')
    checkfileexists([cfg['tdumpdir'], cfg['neobin'], cfg['tdbdir']])
    t = Stopwatch()
    logging.debug('Checking if Neo4j is running')
    if subroutine([path.join(cfg['neobin'], 'neo4j'), 'status']) \
            .stdout.decode('utf-8').lower().startswith('neo4j is running'):
        logging.debug('Shutting down Neo4j')
        subroutine([path.join(cfg['neobin'], 'neo4j'), 'stop'])
    logging.debug('Removing all files in {}'.format(cfg['tdbdir']))
    for f in [path.join(cfg['tdbdir'], f) for f in listdir(cfg['tdbdir'])]:
        remove(f)
    srargs = [path.join(cfg['neobin'], 'neo4j-import'), '--quote', '\'', '--into', cfg['tdbdir'], '--nodes:r',
              ','.join(sorted([f for f in listdir(cfg['tdumpdir']) if f.startswith('res')])), '--nodes:c',
              '.'.join(sorted([f for f in listdir(cfg['tdumpdir']) if f.startswith('contrib')])), '--relationships:a',
              '.'.join(sorted([f for f in listdir(cfg['tdumpdir']) if f.startswith('rela')]))]
    subroutine(srargs)
    logging.info('Creation of Neo4j database finished. Took {}'.format(t.stop()))
    logging.info('Starting up Neo4j server')
    subroutine([path.join(cfg['neobin'], 'neo4j '), 'start'])
    logging.info('Creating index on sysno for :r-labelled nodes')
    driver = GraphDatabase.driver("bolt://localhost", auth=basic_auth(cfg['neousr'], cfg['neopwd']))
    session = driver.session()
    session.run("CREATE CONSTRAINT ON (r:r) ASSERT r.sysno IS UNIQUE")
    session.run("CREATE INDEX ON :r(sysno)")
    session.close()

logging.info('All finished! Took {}'.format(ta.stop()))
