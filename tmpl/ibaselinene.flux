filesize    = "10000";
records     = "20000";
compress    = "false";
extension   = "jsonld";
header      = "true";

[blin]|
read-dir|
open-file|
catch-object-exception|
decode-xml|
handle-marcxml|
filter([245afilter])|
stream-tee| {
    morph([resomorph])|
    change-id|
    encode-esbulk(escapeChars="true", header=header, index=[esindex], type="bibliographicResource")|
    index-esbulk(esNodes=[estcphosts], esClustername=[escluster], recordsPerUpload=records)
}{
    morph([documorph])|
    change-id|
    encode-esbulk(escapeChars="true", header=header, index=[esindex], type="document")|
    index-esbulk(esNodes=[estcphosts], esClustername=[escluster], recordsPerUpload=records)
}{
    morph([itemmorph])|
    split-entities|
    change-id|
    encode-esbulk(escapeChars="true", header=header, index=[esindex], type="item")|
    index-esbulk(esNodes=[estcphosts], esClustername=[escluster], recordsPerUpload=records)
}{
    morph([orgamorph])|
    split-entities|
    change-id|
    encode-esbulk(escapeChars="true", header=header, index=[esindex], type="organisation")|
    index-esbulk(esNodes=[estcphosts], esClustername=[escluster], recordsPerUpload=records)
}{
    morph([persmorph])|
    split-entities|
    change-id|
    encode-esbulk(escapeChars="true", header=header, index=[esindex], type="person")|
    index-esbulk(esNodes=[estcphosts], esClustername=[escluster], recordsPerUpload=records)
};
