compress    = "true";
dirBase     = [dumpbase];
inputDirBase    = dirBase + "/jsonld";
outputDirBase    = dirBase + "/turtle/";


inResource = inputDirBase + "/bibrecord";
inPerson = inputDirBase + "/person";
inOrganisation = inputDirBase + "/organisation";
inItem = inputDirBase + "/items";
inDocument = inputDirBase + "/docrecord";


outResource = outputDirBase + "/bibrecord";
outPerson = outputDirBase + "/person";
outOrganisation = outputDirBase + "/organisation";
outItem = outputDirBase + "/items";
outDocument = outputDirBase + "/docrecord";



extension   = "ttl";

contexts = "https://resources.swissbib.ch/document/context.jsonld###https://resources.swissbib.ch/item/context.jsonld###https://resources.swissbib.ch/person/context.jsonld###https://resources.swissbib.ch/organisation/context.jsonld###https://resources.swissbib.ch/resource/context.jsonld###https://resources.swissbib.ch/work/context.jsonld";


inResource|
read-dir|
open-file|
turtle-producer (context=contexts, type="bibliographicResource", extension=extension, compress=compress, baseOutDir=outResource);



inPerson|
read-dir|
open-file|
turtle-producer (context=contexts, type="person", extension=extension, compress=compress, baseOutDir=outPerson);


inOrganisation|
read-dir|
open-file|
turtle-producer (context=contexts, type="organisation", extension=extension, compress=compress, baseOutDir=outOrganisation);


inItem|
read-dir|
open-file|
turtle-producer (context=contexts, type="item", extension=extension, compress=compress, baseOutDir=outItem);


inDocument|
read-dir|
open-file|
turtle-producer (context=contexts, type="document", extension=extension, compress=compress, baseOutDir=outDocument);

