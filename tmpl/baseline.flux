filesize    = "10000";
records     = "20000";
compress    = "false";
extension   = "jsonld";

[crdirtmp]|
read-dir|
open-file|
catch-object-exception|
decode-xml|
handle-marcxml|
filter([245afilter])|
stream-tee| {
    morph([resomorph])|
    change-id|
    encode-esbulk(escapeChars="true", header="true", index=[esindex], type="bibliographicResource")|
    index-esbulk(esNodes=[estcphosts], esClustername=[escluster], recordsPerUpload=records)
}{
    morph([documorph])|
    change-id|
    encode-esbulk(escapeChars="true", header="true", index=[esindex], type="document")|
    index-esbulk(esNodes=[estcphosts], esClustername=[escluster], recordsPerUpload=records)
}{
    morph([itemmorph])|
    split-entities|
    change-id|
    encode-esbulk(escapeChars="true", header="true", index=[esindex], type="item")|
    index-esbulk(esNodes=[estcphosts], esClustername=[escluster], recordsPerUpload=records)
}{
    morph([orgamorph])|
    split-entities|
    change-id|
    lookup-es(esClustername=[escluster], esNodes=[estcphosts], esIndex=[esindex], esType="organisation")|
    encode-esbulk(escapeChars="true", header="false", index=[esindex], type="organisation")|
    write-esbulk(baseOutDir=[blout], fileSize=filesize, jsonCompliant="true", type="organisation", compress=compress, extension=extension)
}{
    morph([persmorph])|
    split-entities|
    change-id|
    lookup-es(esClustername=[escluster], esNodes=[estcphosts], esIndex=[esindex], esType="person")|
    encode-esbulk(escapeChars="true", header="false", index=[esindex], type="person")|
    write-esbulk(baseOutDir=[blout], fileSize=filesize, jsonCompliant="true", type="person", compress=compress, extension=extension)
//}{
//    filter([workfilter])|
//    morph([workmorph1])|
//    stream-to-triples(redirect="true")|
//    sort-triples(by="all")|
//    collect-triples|
//    morph([workmorph2])|
//    split-entities|
//    encode-esbulk(escapeChars="true", header="true", index=[esindex], type="work")|
//    index-esbulk(esNodes=[estcphosts], esClustername=[escluster], recordsPerUpload=records)
};
