records="50000";
header="false";
escapechars ="true";
filesize    = "50000";
compress    = "true";
extension   = "jsonld";


[elin]|
open-file|
decode-ntriples(unicodeEscapeSeq="true")|
stream-to-triples(redirect="true")|
collect-triples|
morph([elmorph])|
stream-tee| {
    filter([elpersfilter])|
    encode-esbulk(escapeChars=escapechars, header=header)|
    write-esbulk(baseOutDir=[outdirDumpPerson], fileSize=filesize, jsonCompliant="true", type="person", compress=compress, extension=extension)
}{
    filter([elorgafilter])|
    encode-esbulk(escapeChars=escapechars, header=header)|
    write-esbulk(baseOutDir=[outdirDumpOrganisation], fileSize=filesize, jsonCompliant="true", type="organisation", compress=compress, extension=extension)
};
