filesize    = "10000";
records     = "20000";
compress    = "false";
extension   = "jsonld";

[blin]|
read-dir|
open-file|
catch-object-exception|
decode-xml|
handle-marcxml|
filter([245afilter])|
stream-tee| {
    morph([resomorph])|
    change-id|
    encode-esbulk(escapeChars="true", header="true", index=[esindex], type="bibliographicResource")|
    index-esbulk(esNodes=[estcphosts], esClustername=[escluster], recordsPerUpload=records)
}{
    morph([documorph])|
    change-id|
    encode-esbulk(escapeChars="true", header="true", index=[esindex], type="document")|
    index-esbulk(esNodes=[estcphosts], esClustername=[escluster], recordsPerUpload=records)
}{
    morph([itemmorph])|
    split-entities|
    change-id|
    encode-esbulk(escapeChars="true", header="true", index=[esindex], type="item")|
    index-esbulk(esNodes=[estcphosts], esClustername=[escluster], recordsPerUpload=records)
}{
    morph([orgamorph])|
    split-entities|
    change-id|
    encode-esbulk(escapeChars="true", header="false", index=[esindex], type="organisation")|
    write-esbulk(baseOutDir=[blout], fileSize=filesize, jsonCompliant="true", type="organisation", compress=compress, extension=extension)
}{
    morph([persmorph])|
    split-entities|
    change-id|
    encode-esbulk(escapeChars="true", header="false", index=[esindex], type="person")|
    write-esbulk(baseOutDir=[blout], fileSize=filesize, jsonCompliant="true", type="person", compress=compress, extension=extension)
};
