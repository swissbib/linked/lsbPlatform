filesize    = "50000";
records     = "50000";
compress    = "true";
extension   = "jsonld";


[blindump]|
read-dir|
open-file|
catch-object-exception|
decode-xml|
handle-marcxml|
filter([245afilter])|
stream-tee| {
    morph([resomorph])|
    change-id|
    encode-esbulk(escapeChars="true", header="false")|
    write-esbulk(jsonCompliant="true", filesize=filesize, compress=compress, baseOutDir=[outdirDumpBib], type="bibliographicResource", extension=extension)
}{

    morph([documorph])|
    change-id|
    encode-esbulk(escapeChars="true", header="false")|
    write-esbulk(jsonCompliant="true", filesize=filesize, compress=compress, baseOutDir=[outdirDumpDoc], type="document", extension=extension)

}{
    morph([itemmorph])|
    split-entities|
    change-id|
    encode-esbulk(escapeChars="true", header="false")|
    write-esbulk(jsonCompliant="true", filesize=filesize, compress=compress, baseOutDir=[outdirDumpItem], type="item", extension=extension)
}{
    morph([orgamorph])|
    split-entities|
    change-id|
    encode-esbulk(escapeChars="true", header="false", type="organisation")|
    write-esbulk(baseOutDir=[blout], fileSize=filesize, jsonCompliant="true", type="organisation", compress="false", extension=extension)
}{
    morph([persmorph])|
    split-entities|
    change-id|
    encode-esbulk(escapeChars="true", header="false", type="person")|
    write-esbulk(baseOutDir=[blout], fileSize=filesize, jsonCompliant="true", type="person", compress="false", extension=extension)
};

