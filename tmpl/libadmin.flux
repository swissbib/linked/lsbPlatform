[libadminsource]|
open-http(encoding="UTF-8")|
read-json-object|
decode-json|
morph([libadminmorph])|
split-entities|
change-id|
encode-esbulk(escapeChars="false", header="true", index=[esindex], type="organisation")|
index-esbulk(esNodes=[estcphosts], esClustername=[escluster], recordsPerUpload="100");
