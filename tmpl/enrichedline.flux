records="20000";
header="true";
escapechars="true";

[elin]|
open-file|
decode-ntriples(unicodeEscapeSeq="true")|
stream-to-triples(redirect="true")|
filter-duplicate-objects|
collect-triples|
morph([elmorph])|
change-id|
stream-tee| {
    filter([elpersfilter])|
    encode-esbulk(escapeChars=escapechars, header=header, index=[esindex], type="person")|
    index-esbulk(esNodes=[estcphosts], esClustername=[escluster], recordsPerUpload=records)
}{
    filter([elorgafilter])|
    encode-esbulk(escapeChars=escapechars, header=header, index=[esindex], type="organisation")|
    index-esbulk(esNodes=[estcphosts], esClustername=[escluster], recordsPerUpload=records)
};
