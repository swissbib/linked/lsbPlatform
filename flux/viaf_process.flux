records="20000";
header="true";
escapechars="true";

"/in/viaf-20181007-clusters-rdf.nt"|
open-file|
decode-ntriples(unicodeEscapeSeq="true")|
stream-to-triples(redirect="true")|
log-object("Triples: ")|
filter-triples(subjectPattern=".*", predicatePattern="http://www.w3.org/2000/01/rdf-schema#comment", objectPattern=".*", passMatches="false")|
collect-triples|
change-id|
encode-esbulk(escapeChars=escapechars, header=header, index="test", type="viaf")|
index-esbulk(esNodes="localhost:9300", esClustername="elasticsearch", recordsPerUpload=records);
